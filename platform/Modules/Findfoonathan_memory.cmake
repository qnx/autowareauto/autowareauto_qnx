find_package(foonathan_memory CONFIG QUIET)
if(NOT foonathan_memory_FOUND)
  find_path(foonathan_memory_INCLUDE_DIR foonathan/memory PATH_SUFFIXES foonathan_memory)
  find_library(foonathan_memory_LIBRARY NAMES foonathan_memory foonathan_memory-0.7.0)
  include (FindPackageHandleStandardArgs)
  find_package_handle_standard_args (foonathan_memory DEFAULT_MSG
    foonathan_memory_LIBRARY
    foonathan_memory_INCLUDE_DIR)
  mark_as_advanced (foonathan_memory_LIBRARY
    foonathan_memory_INCLUDE_DIR)
else()
  if(TARGET foonathan_memory)
    set(foonathan_memory_LIBRARY foonathan_memory)
  elseif(TARGET foonathan_memory::foonathan_memory)
    set(foonathan_memory_LIBRARY foonathan_memory::foonathan_memory)
  endif()
  list(APPEND foonathan_memory_TARGETS ${foonathan_memory_LIBRARY})
endif()

